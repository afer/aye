from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import (QWidget, QLabel, QLineEdit, QVBoxLayout, QHBoxLayout,
                               QTextEdit, QGridLayout, QApplication, QPushButton, QComboBox, QTableView,)
from PySide2.QtCore import SIGNAL
from skimage import io
import sys
import os
### Remove from here

from imageProcessing import * 
from widgets import *
from metadata2df import *
# from pandasModel import *
from controllers import *
## Remove until here

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)

    # data_dir="/home/alan/Desktop/UoE/softDev/annotGUI/curationGUI/testPoses/"
    ex = mainController()
    
    sys.exit(app.exec_())
