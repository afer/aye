# Note: TODO Fix using relative selection. When the frame is small it only uses the relative position, not the real numer
from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import (QWidget, QLabel, QLineEdit, 
                               QTextEdit, QGridLayout, QApplication, QPushButton, QComboBox, QTableView)
from PySide2.QtCore import QObject, Signal, Slot, QThread, QSignalBlocker

from skimage import io
from skimage.io._plugins.pil_plugin import ndarray_to_pil

import numpy as np
from widgets import *
from imageProcessing import *
from metadata2df import  *

class localSignals(QObject):
    signal_list = Signal(list)
    signal_str = Signal(str)

class locationController(QObject):
    def __init__(self, metadata_fields, df=None):
        self.df = df
        self.metadata_fields = metadata_fields
        self.location_frame = locationFrame(metadata_fields = self.metadata_fields)
        self.selection_index = [0 for internal_widget in self.location_frame.internal_widgets]
        self.signals = localSignals()
        self.init_controller()

    @Slot(int, str)
    def receive_changed_selection(self, new_index, metadata_field):
        metadata_index = self.metadata_fields.index(metadata_field)

        # Update index
        self.selection_index[metadata_index] = new_index
        for i in range(metadata_index+1, len(self.metadata_fields)):
            self.selection_index[i] = 0

        new_index = self.selection_index
        self.update_location(new_index, metadata_field)

    def init_controller(self):
        self.location_frame.set_list_widgets()
        self.connect_signals() # This are the signals sent by the viewer #TODO Move this signals to the controller
        # self.set_local_signals() # Set local signals to communicate with other controllers
        self.location_frame.set_layout()

    def fill_frame(self):
        self.set_sublists()
        self.set_selection()
        # Set the new sublists as zero
        self.update_location([0 for field in self.metadata_fields], self.metadata_fields[0])

        #Manually update the first column.
        #Not working properly for some reason. TODO: Check for n>1
    def set_selection(self):
        self.selection = []
        for i, internal_widget in enumerate(self.location_frame.internal_widgets):
            internal_widget.selection_index = internal_widget.setItemSelected(
                internal_widget.item(self.selection_index[i]),1)
            # TODO: Fix this to be ints and not QItems
            self.selection.append(self.location_frame.sublists[i][self.selection_index[i]])

    def set_sublists(self):
        '''
        We consecutively filter of our location dataframe using the selected values
        '''
        visual_sublists = [np.sort(self.df[self.metadata_fields[0]].unique())]
        sub_dfs = [self.df.loc[self.df[self.metadata_fields[0]]==visual_sublists[0][self.selection_index[0]]]]
        for field, val in zip(self.metadata_fields[1:], self.selection_index[1:]):
            unique_values = np.sort(sub_dfs[-1][field].unique())
            visual_sublists.append(unique_values)

            sub_df = sub_dfs[-1].loc[sub_dfs[-1][field]==unique_values[val]]
            sub_dfs.append(sub_df)

        self.location_frame.sublists = [[int(item) for item in sublist]
                                        for sublist in visual_sublists]

    def update_location(self, new_index, metadata_field):
        # Block signals for list change
        for internal_widget in self.location_frame.internal_widgets:
            internal_widget.blockSignals(True)

        # Update lists, models and selection
        self.set_sublists()
        self.location_frame.update_models(metadata_field, new_index)
        self.selection = [self.location_frame.sublists[field_index][element_index]
                          for field_index, element_index in enumerate(new_index)]
        # Unblock signals after updating selection and list
        for internal_widget in self.location_frame.internal_widgets:
            internal_widget.blockSignals(False)

        self.emit_location_list()

    def emit_location_list(self):
        '''
        Sends the current position for the imageController class to catch and change the image
        '''
        self.signals.signal_list.emit(self.selection)

    def connect_signals(self):
            #Connect signals to slots
        for internal_widget in self.location_frame.internal_widgets:
            internal_widget.currentRowChanged.connect(internal_widget.emit_current_selection)
            internal_widget.signals.signal_int_str.connect(self.receive_changed_selection)

    def update_sublists(self):
        self.location_frame = locationFrame(metadata_fields = self.metadata_fields)

    @Slot(str)
    def select_next_value(self, field_to_change):
        metadata_index = self.metadata_fields.index(field_to_change)
        # Find the index for the current selected value
        local_index = self.location_frame.sublists[metadata_index].index(self.selection[metadata_index])
        if metadata_index >= 0:
            if local_index < len(self.location_frame.sublists[metadata_index])-1:
                # Update selection and their index
                self.selection_index[metadata_index] = local_index+1
                self.selection[metadata_index] = self.location_frame.sublists[metadata_index][self.selection_index[metadata_index]]

                widget_to_change = self.location_frame.internal_widgets[metadata_index]
                widget_to_change.setItemSelected(widget_to_change.item(local_index+1),1)
                self.update_location(self.selection_index, field_to_change)
            else:
                # print("Changing " + self.metadata_fields[metadata_index])
                if metadata_index > 0:
                    self.selection_index[metadata_index] = 0
                    self.selection[metadata_index] = self.location_frame.sublists[metadata_index][0]

                    widget_to_change = self.location_frame.internal_widgets[metadata_index]
                    widget_to_change.setItemSelected(widget_to_change.item(0),1)

                    field_to_change = self.metadata_fields[metadata_index-1] 
                    self.select_next_value(field_to_change)
                else:
                    print("We are in the endgame now")
            print(self.selection_index)


class imagesController(QObject):
    def __init__(self, metadata_fields,df=None, selection = [1,8,1,1],
                 raw_channels=['GFP'], cropped_channels = ['GFP'],
                 segoutlines=True):
        self.df = df
        self.raw_channels = raw_channels
        self.cropped_channels = cropped_channels
        self.metadata_fields = metadata_fields
        self.segoutlines = 1 if segoutlines==True else 0
        self.shown_images = [None for i in range(len(self.raw_channels)
                                                 + len(self.cropped_channels)
                                                 * self.segoutlines)]
        self.images_frame = imagesFrame(n_images = len(self.shown_images))

        self.selection = selection
        self.init_controller()

    def init_controller(self):
        # Sets up an empty image frame
        self.images_frame.set_shown_images()

    def find_images(self):
        tmp_df = self.df
        for field, val in zip(self.metadata_fields[:-1], self.selection[:-1]):
            tmp_df = tmp_df.loc[tmp_df[field]==val]
        self.n_cells = len(tmp_df)
        self.cell_ids = list(tmp_df['cell_id'].astype(int).values)
        self.selected_cell_index = self.cell_ids.index(self.selection[-1])
        assert self.selected_cell_index<self.n_cells, "cell index out of bounds"
        self.df_metadata = {field : int(tmp_df[field].values[0]) for field in self.metadata_fields}
        return([tmp_df['filename'].values[i] for i in range(self.n_cells)])

    def read_images(self):
        # Function that reads the images name and content and organises them into two separate dictionaries
        self.raw_image_dict= {}

        for channel in np.unique(self.raw_channels + self.cropped_channels + ['segoutlines']):
            self.raw_image_dict[channel] = io.imread(self.basename + '_' + channel + '.png')

    def process_images(self):
        # Function that uses the defined cropped image requirements, crops and processes the normalisation of pixel intensities
        self.metadata_dict = {}
        self.metadata_dict['segoutlines'] = {'nCells':self.n_cells, 'height':117, 'width':117*self.n_cells}

        #Process images to crop, from the image loaded with scikit-image to the Qt pixmap (scimage -> pillow -> qt pixmap)
        self.norm_pixmap = {}
        self.new_shown_images = []
        for channel in self.cropped_channels:
            self.metadata_dict[channel] = {'nStacks':5, 'height':117, 'width':585}
            masked_dict = generateMaskedArrays(self.raw_image_dict[channel], self.raw_image_dict['segoutlines'], self.metadata_dict[channel], self.metadata_dict['segoutlines'])
            norm_image = normalizeArray(masked_dict[self.selected_cell_index])
            norm_qt_image = ImageQt(ndarray_to_pil(norm_image))
            self.norm_pixmap[channel] = QtGui.QPixmap.fromImage(norm_qt_image)

        # Using two for loops to have the cropped images close to their raw counterparts
        self.raw_pixmap = {}

        for channel in np.unique(self.raw_channels + self.cropped_channels):
            if channel in self.cropped_channels:
                self.new_shown_images.append(self.norm_pixmap[channel])

            #We also do the necessary conversions in place for the raw images
            if channel in self.cropped_channels:
                qt_image = ImageQt(ndarray_to_pil(normalizeArray(self.raw_image_dict[channel])))
                self.raw_pixmap[channel] = QtGui.QPixmap.fromImage(qt_image)
                self.new_shown_images.append(self.raw_pixmap[channel])

    @Slot(list)
    def update_images(self, arg_list):
        self.selection = arg_list
        fname = self.find_images()[self.selected_cell_index]
        self.basename = '_'.join(fname.split('_')[:-1])
        self.read_images()
        self.process_images()
        for i in range(len(self.shown_images)):
            self.shown_images[i] = self.new_shown_images[i]

        self.images_frame.shown_images = self.shown_images
        self.images_frame.update_shown_images()

class annotationController(QObject):
    def __init__(self, annotation_frame=None):
        super().__init__()
        self.init_controller()

    @Slot(str)
    def decision_button_clicked(self, arg_str):
        # print("String detected with value ", arg_str)
        self.decision, self.next_field = self.switch_case(arg_str)
        # print("Decision: " + str(self.decision))
        # print("Next field: " + str(self.next_field))
        self.emit_decision_made()


    def switch_case(self, arg_str):
        switcher = {
            'All'      : [True, 'tp'],
            'Yes'      : [True, 'cell_id'],
            'None'     : [False, 'tp'],
            'No'       : [False, 'cell_id'],
            'Skip all' : [None, 'tp'],
            'Skip'     : [None, 'cell_id']
        }
        return(switcher.get(arg_str, "Error"))

    def init_controller(self):
        self.annotation_frame = annotationFrame()

        self.signals = localSignals(self)
        self.connect_signals()

    def connect_signals(self):
            self.annotation_frame.signals.signal_str.connect(self.decision_button_clicked)
        
    def emit_decision_made(self):
        self.signals.signal_list.emit([self.decision, self.next_field])

class ioController(QObject):
    def __init__(self, out_path=None):
        self.io_frame = ioFrame(out_path = out_path)
        self.annotated_df = pd.DataFrame()
        self.out_path = out_path

        self.init_controller()

    @Slot(str)
    def save_annotated_df(self, arg_str):
        self.out_path = arg_str if arg_str != None else self.out_path
        self.annotated_df.to_csv(self.out_path + '/annotations.tsv', sep='\t')

    def update_df(self, arg_list):
        for row in arg_list:
            self.annotated_df = self.annotated_df.append(row, ignore_index = True)

    def init_controller(self):
        self.input_folder = None
        self.io_frame.dialog_output_dir.signals.signal_str.connect(self.save_annotated_df)

class mainController(QObject):
    # TODO Rearrange the default input folder and add an interface for its selection
    def __init__(self, input_folder = None, metadata_fields =
                 ['position', 'trap', 'tp', 'cell_id'], annotation_frame =
                 None, images_frame = None, shown_images= None):
        self.input_folder = input_folder # This is used to obtain df
        self.df =  generate_position_list_from_folder(input_folder) if input_folder != None else None
        self.metadata_fields = metadata_fields

        self.location_controller= locationController(df=self.df, metadata_fields = self.metadata_fields)
        self.images_controller = imagesController(df=self.df, metadata_fields = self.metadata_fields)
        self.annotation_controller = annotationController()
        self.io_controller = ioController(out_path = self.input_folder)
        self.init_main_controller()

    def init_main_controller(self):
        self.main_window = mainWindow(images_frame=
                                      self.images_controller.images_frame,
                                      location_frame =
                                      self.location_controller.location_frame,
                                      annotation_frame =
                                      self.annotation_controller.annotation_frame,
                                      io_frame = self.io_controller.io_frame)
        self.connect_signals()
        self.main_window.show()


    @Slot(str)
    def load_input(self, arg_str):
        # Load a prediction dataframe if the file is a csv, else load whole folder
        self.df = pd.read_csv(arg_str, index_col=0) if arg_str.endswith('.csv') else \
            generate_position_list_from_folder(arg_str[:arg_str.rfind('/')])

        assert len(self.df.index) > 0, "No image found in folder"

        self.location_controller.df = self.df
        self.images_controller.df = self.df

        self.location_controller.fill_frame()

    @Slot(list)
    def pass_annotation_result(self, arg_list):
        selection = self.location_controller.selection
        annotation_value = arg_list[0]
        next_field = arg_list[1]

        # We recover the position by using the general dataframe
        if annotation_value is not None:
            sub_df = self.df
            for field, val in zip(self.metadata_fields[:-1], self.location_controller.selection[:-1]):
                sub_df = sub_df.where(sub_df[field]==val).dropna()

            sub_df['annotation'] = [annotation_value for i in range(sub_df.shape[0])]

            if next_field==self.metadata_fields[-1]:
                list_dfs_annotated = [sub_df.where(sub_df[self.metadata_fields[-1]]==self.location_controller.selection[-1]).dropna()]
                assert len(list_dfs_annotated) ==1, print(list_dfs_annotated)
            elif next_field==self.metadata_fields[-2]:
                list_dfs_annotated = [sub_df.iloc[i] for i in range(sub_df.shape[0])]
            else:
                print("Error while checking for next_field")

            self.io_controller.update_df(list_dfs_annotated)
        self.location_controller.select_next_value(next_field)

    def connect_signals(self):
        # Update images when location changes
        self.location_controller.signals.signal_list.connect(self.images_controller.update_images)

        # Update dataframe when annotation button pressed. This also selects next value
        self.annotation_controller.signals.signal_list.connect(self.pass_annotation_result)

        # Load folder to the necessary objects and refresh them
        self.io_controller.io_frame.dialog_input_dir.signals.signal_str.connect(self.load_input)

