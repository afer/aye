import os
import json
import pandas as pd
import numpy as np
'''
Code obtained from 
https://blender.stackexchange.com/questions/35504/read-image-metadata-from-python

'''

def find_pngs(path, rec=False):
    '''
    Reads a given path and returns the name of all png images in it.
    path: path to files
    rec: Look for subfolder recursively
    '''
    result = list(Path(".").rglob("*.[tT][xX][tT]"))
    pngs = (os.path.join(path, elem)
                for elem in os.listdir(path) if elem.endswith(".png"))
    return pngs

def chunk_iter(data):
    total_length = len(data)
    end = 4

    while(end + 8 < total_length):     
        length = int.from_bytes(data[end + 4: end + 8], 'big')
        begin_chunk_type = end + 8
        begin_chunk_data = begin_chunk_type + 4
        end = begin_chunk_data + length

        yield (data[begin_chunk_type: begin_chunk_data],
               data[begin_chunk_data: end])

def read_metadata(png):
    '''
    Function that reads metadata from a single png image and returns the type 'tEXt' metadata and the filename with complete path as a dictionary. '''
    with open(png, 'rb') as fobj:
        data = fobj.read()

    # check signature
    assert data[:8] == b'\x89\x50\x4E\x47\x0D\x0A\x1A\x0A'

    for chunk_type, chunk_data in chunk_iter(data):
        if chunk_type == b'tEXt':
            metadata= json.loads(chunk_data.decode('iso-8859-1').split('\0')[1])
            metadata['filename'] = png
    return metadata

def classify_metadata(metadatas):
    '''
    Takes a list of metadata in form of dictionaries and classifies them in a dictionary where the keys are the corresponding channel of the image.'''
    channels_found = np.unique([data['channel'] for data in metadatas])

    colnames_image = ['experimentID', 'position', 'trap', 'tp', 'channel', 'cellLabels', 'buds', 'ntiles', 'filename']
    colnames_segmentation = ['experimentID', 'position', 'trap', 'tp', 'channel', 'ntiles', 'filename']

    metadata_dict = {}
    for channel in channels_found:
        used_cols = colnames_segmentation if channel == 'segoutlines' else colnames_image

        metadata_postreduction = []
        for data in metadatas:
            if data['channel']==channel:
                metadata_postreduction.append(data)
        metadata_dict[channel] = pd.DataFrame(metadata_postreduction).set_index(['position', 'trap', 'tp'])
    return metadata_dict

def reduce_to_intersection(metadata_dict, channels='all'):
    '''
    Function that takes a dictionary of dataframes containing the metadata of images that have the position, trap and timepoint as multiindexes and returns the minimal set of pos-trap-timepoint shared among them (The intersection, if seen as sets).
    '''
    minimal_df_dict = {}
    if channels=='all':
        set_intersection = list(set(metadata_dict[list(metadata_dict.keys())[0]].index).intersection(*[set(data.index) for data in metadata_dict.values()]))
        for channel in metadata_dict.keys():
            main_set = set(metadata_dict[channel].index)
            minimal_df_dict[channel] = metadata_dict[channel].loc[set_intersection]
    return minimal_df_dict

def generate_position_list(reduced):
    '''
    Function that takes as an input a dictionary of dataframes containing the metadata of images with position, trap and timepoint as multiindexes (previously reduced using "reduce_to_minimal")and returns a list of indexes and cell id.
    '''

    cell_dict_list = []

    for index,row in reduced['segoutlines'].iterrows():
        if row['cellLabels']!=None:
            cell_ids = [row['cellLabels']] if isinstance(row['cellLabels'], int) else row['cellLabels']
            for cell_id in cell_ids:
                cell_dict_list.append({'experimentID':row['experimentID'],'position': index[0], 'trap': index[1], 'tp': index[2], 'cell_id': cell_id, 'filename': row['filename']})

    return pd.DataFrame(cell_dict_list)

def get_poses(path):
    '''
    Function that takes as an input a folder and returns a dataframe with the positions, traps, and timepoints
    '''

    pngs = find_pngs(path)
    metadatas = [read_metadata(png) for png in pngs]
    metadata_dict = classify_metadata(metadatas)
    reduced = reduce_to_intersection(metadata_dict)
    position_list = generate_position_list(reduced)
    return position_list


