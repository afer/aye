'''
Code for reading/writing metadata from/to the PNG images
'''

import os
import json
import pandas as pd
'''
Code obtained from 
https://blender.stackexchange.com/questions/35504/read-image-metadata-from-python

Panda DataFrame to model function from
https://stackoverflow.com/questions/44603119/how-to-display-a-pandas-data-frame-with-pyqt5-pyside2
'''
class DataFrameModel(QtCore.QAbstractTableModel):
    DtypeRole = QtCore.Qt.UserRole + 1000
    ValueRole = QtCore.Qt.UserRole + 1001

    def __init__(self, df=pd.DataFrame(), parent=None):
        super(DataFrameModel, self).__init__(parent)
        self._dataframe = df

    def setDataFrame(self, dataframe):
        self.beginResetModel()
        self._dataframe = dataframe.copy()
        self.endResetModel()

    def dataFrame(self):
        return self._dataframe

    dataFrame = QtCore.pyqtProperty(pd.DataFrame, fget=dataFrame, fset=setDataFrame)

    @QtCore.pyqtSlot(int, QtCore.Qt.Orientation, result=str)
    def headerData(self, section: int, orientation: QtCore.Qt.Orientation, role: int = QtCore.Qt.DisplayRole):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self._dataframe.columns[section]
            else:
                return str(self._dataframe.index[section])
        return QtCore.QVariant()

    def rowCount(self, parent=QtCore.QModelIndex()):
        if parent.isValid():
            return 0
        return len(self._dataframe.index)

    def columnCount(self, parent=QtCore.QModelIndex()):
        if parent.isValid():
            return 0
        return self._dataframe.columns.size

    def data(self, index, role=QtCore.Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < self.rowCount() \
            and 0 <= index.column() < self.columnCount()):
            return QtCore.QVariant()
        row = self._dataframe.index[index.row()]
        col = self._dataframe.columns[index.column()]
        dt = self._dataframe[col].dtype

        val = self._dataframe.iloc[row][col]
        if role == QtCore.Qt.DisplayRole:
            return str(val)
        elif role == DataFrameModel.ValueRole:
            return val
        if role == DataFrameModel.DtypeRole:
            return dt
        return QtCore.QVariant()

    def roleNames(self):
        roles = {
            QtCore.Qt.DisplayRole: b'display',
            DataFrameModel.DtypeRole: b'dtype',
            DataFrameModel.ValueRole: b'value'
        }
        return roles

# chunk generator
def chunkIter(data):
    total_length = len(data)
    end = 4

    while(end + 8 < total_length):     
        length = int.from_bytes(data[end + 4: end + 8], 'big')
        begin_chunk_type = end + 8
        begin_chunk_data = begin_chunk_type + 4
        end = begin_chunk_data + length

        yield (data[begin_chunk_type: begin_chunk_data],
               data[begin_chunk_data: end])

pngs = (os.path.join(imPath, elem)
            for elem in os.listdir(imPath) if elem.endswith(".png"))

metadataDf=pd.DataFrame(columns = ["experimentID", "position", "trap", "tp", "channel", "cellLabels", "buds", "ntiles", "filename"])

def readMetadata(png):
    with open(png, 'rb') as fobj:
        data = fobj.read()

    # check signature
    assert data[:8] == b'\x89\x50\x4E\x47\x0D\x0A\x1A\x0A'

    for chunk_type, chunk_data in chunk_iter(data):
        # print("chunk type: %s" % chunk_type.decode())
        if chunk_type == b'tEXt':
            # print("--chunk data:", *chunk_data.decode('iso-8859-1').split('\0'))
            metadata= json.loads(chunk_data.decode('iso-8859-1').split('\0')[1])
    return metadata

metadatas = [readMetadata(png) for png in pngs]

imagesDF = pd.DataFrame()
outlinesDF = pd.DataFrame()
# Fill dataframe with metadata

expIds = [] 
positions = []
traps = []
tps = []
channels = []
ntiles = []
# for metadata, filename in zip(metadatas, pngs):
