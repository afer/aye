from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import (QWidget, QLabel, QLineEdit, QListView, QVBoxLayout, QListWidget, QListWidgetItem, QHBoxLayout, QTextEdit, QGridLayout, QApplication, QPushButton, QComboBox, QTableView, QGraphicsPixmapItem, QSizePolicy, QFileDialog)
from PySide2.QtCore import QObject, Signal, Slot, QRectF
from PySide2.QtGui import QStandardItemModel, QStandardItem, QPainter, QPixmap
import random
import numpy as np
 
featureNames = ["Aggregates"]
featureTypes = ["Boolean"]

class localSignals(QObject):
    signal_int_str = Signal(int, str)
    signal_str = Signal(str)

class comboFeatureType(QComboBox):

    def __init__(self):
        super().__init__()

        self.initWidget()

    def initWidget(self):
        comboFeatureName = QComboBox(self)
        for featureType in featureTypes:
            comboFeatureType.addItem(featureType)

class comboFeatureName(QComboBox):

    def __init__(self):
        super().__init__()

        self.initWidget()

    def initWidget(self):
        comboFeatureName = QComboBox(self)
        for featureName in featureNames:
            comboFeatureName.addItem(featureName)

class listView(QListWidget):
    def __init__(self, selected_idx=0, name=None, vals=None):
        super().__init__()

        self.name = name
        self.vals = vals
        self.selected_idx = selected_idx

        self.signals = localSignals()

        self.init_widget()

    def init_widget(self):
        self.generate_data()

    def generate_data(self):
        for val in self.vals:
            item = QListWidgetItem(str(val))
            self.addItem(item)
        self.setCurrentRow(self.selected_idx)

    def update_model_and_view(self):
        self.clear()
        self.generate_data()
        for index, value in enumerate(self.vals):
            item = self.item(index)
            item.setText(str(value))
        self.setCurrentRow(self.selected_idx)
        self.update()

    def emit_current_selection(self):
        current_row = int(self.currentRow())
        current_row = current_row if current_row >=0 else 0
        self.signals.signal_int_str.emit(current_row, self.name)

class locationFrame(QtWidgets.QGroupBox):
    def __init__(self, metadata_fields):
        super().__init__()
        self.setTitle("Cell location")
        self.sublists = [[''] for field in metadata_fields]
        self.metadata_fields = metadata_fields
        self.internal_widgets = [None for i in range(len(metadata_fields))]

    def set_layout(self):
        '''Define the layout at startup.
        '''
        self.layout = QGridLayout()
        for i,field in enumerate(self.metadata_fields):
            self.layout.addWidget(QLabel(field), 0, i, 1, 1)
            self.layout.addWidget(self.internal_widgets[i], 1, i, 1, 1)
        self.setLayout(self.layout)

    def set_list_widgets(self):
        '''Sets the internal widgets at startup.
        '''
        for i, (field, sublist) in enumerate(zip(self.metadata_fields, self.sublists)):
            self.internal_widgets[i] = listView(name=field, vals=sublist)

    def update_models(self, metadata_field, new_indices):
        ''' Update models for all sublists with information
        self: QGroupBox containing the lists with locations
        metadata_field:
        new_indices:
        '''
        metadata_idx = self.metadata_fields.index(metadata_field)

        # We only update the lists downstream in the hierarchy
        for i,(internal_widget,  new_idx) in enumerate(zip(
                self.internal_widgets[metadata_idx:],
                new_indices[metadata_idx:])):
            internal_widget.vals = self.sublists[metadata_idx+i]
            internal_widget.selected_idx = new_idx if i==0 else 0
            internal_widget.update_model_and_view()

class imagesFrame(QtWidgets.QFrame):
    def __init__(self, parent=None, shown_images=[None], n_images = 1, min_width=585, min_height=117):
        super().__init__()
        # self.setTitle("Image viewer")
        self.shown_images =  shown_images
        self.n_images = n_images
        # self.image_display = [QLabel() for i in range(self.n_images)]
        self.image_holder = QLabel()

        self.size_policy = QSizePolicy(
            QSizePolicy.Preferred, QSizePolicy.Preferred)
        self.setSizePolicy(self.size_policy)

        self.set_shown_images()

    def set_shown_images(self):
        ## Shown_Images visualization
        self.layout = QGridLayout()
        self.image_holder.setSizePolicy(self.size_policy)
        self.layout.addWidget(self.image_holder,0,0,1,1)

        self.setLayout(self.layout)

    def set_paint(self):
        self.width = np.min([image.size().width() for image in self.shown_images])
        self.height = np.sum([image.size().height() for image in self.shown_images])

        # This will make a single image work
        self.setMinimumWidth(self.width)
        self.setMinimumHeight(self.height * self.n_images)

        pm = QtGui.QPixmap(self.width,self.height)

        rectangles = []
        painter = QtGui.QPainter(pm)
        for i,image in enumerate(self.shown_images):
            rectangle = QtCore.QRectF(0,image.size().height()*i,image.size().width(),image.size().height())
            painter.drawPixmap(rectangle, image, QtCore.QRectF(image.rect()))
        painter.end()

        self.image_holder.setPixmap(pm)

    def update_shown_images(self):
        self.set_paint()

class annotationFrame(QtWidgets.QGroupBox):
    def __init__(self, parent=None, annot_type='Boolean', button_options = ['Yes', 'No', 'All', 'None', 'Skip all', 'Skip']):
        super().__init__()
        self.annot_type=annot_type
        self.button_options = button_options

        self.init_frame()
        self.set_controls()

    def init_frame(self):
        self.setTitle("Annotation control")
        self.signals = localSignals()

    def set_controls(self):
        if self.annot_type=='Boolean':
            self.internal_widgets = [None for i in self.button_options]
            self.layout = QGridLayout()

            # Set buttons layout for always having two columns
            for i, button_content in enumerate(self.button_options):
                button_position = len(self.button_options)
                position_xaxis = int(i % 2 ==0)
                position_yaxis = int(i/2)

                self.internal_widgets[i] = QPushButton(button_content)
                self.internal_widgets[i].setText(button_content)
                self.layout.addWidget(self.internal_widgets[i], position_yaxis, position_xaxis, 1, 1)
                self.internal_widgets[i].clicked.connect(self.emit_clicked_button)

            self.setLayout(self.layout) 

    def emit_clicked_button(self):
        sending_button = self.sender()
        self.signals.signal_str.emit(sending_button.text())

class ioFrame(QtWidgets.QGroupBox):
    def __init__(self, input_path=None, out_path=None):
        super().__init__()
        self.input_path = input_path
        self.out_path = out_path

        self.init_frame()

        self.setTitle("I/O Control")

    def init_frame(self):
        # Dialog widget
        # Input
        self.dialog_input_dir = QFileDialog()
        self.dialog_input_dir.setFileMode(QFileDialog.Directory)
        self.dialog_input_dir.signals = localSignals()

        # Output
        self.dialog_output_dir = QFileDialog()
        self.dialog_output_dir.signals = localSignals()
        self.dialog_output_dir.setFileMode(QFileDialog.Directory)

        # Select folder
        self.button_browse = QPushButton()
        self.button_browse.setText("Find Folder")

        self.label_input = QLabel()
        self.label_input_title = QLabel()
        self.label_input_title.setText("Input folder:")

        # Save curated data
        self.button_save = QPushButton()
        self.button_save.setText("Save annotations")

        # Connect buttons to functions
        self.button_browse.clicked.connect(self.get_input)
        self.button_save.clicked.connect(self.generate_outfile)

        self.layout= QGridLayout()

        self.layout.addWidget(self.button_browse, 0,0,1,1)
        self.layout.addWidget(self.button_save, 0,1,1,1)
        self.layout.addWidget(self.label_input_title, 1,0,1,2)
        self.layout.addWidget(self.label_input, 2,0,1,2)

        self.setLayout(self.layout)

    def get_input(self):
        self.input_dir = self.dialog_input_dir.getOpenFileName(self, 'Browse file or folder') 
        if self.input_dir:
            self.dialog_input_dir.signals.signal_str.emit(self.input_dir[0])
            self.label_input.setText(self.input_dir[0])

    def generate_outfile(self):
        self.output_dir = self.dialog_output_dir.getExistingDirectory(self, 'Set annotation folder')
        self.dialog_output_dir.signals.signal_str.emit(self.output_dir)

class mainWindow(QtWidgets.QWidget):

    def __init__(self, images_frame=None, location_frame=None, annotation_frame = None, io_frame = None):
        super().__init__()

        self.images_frame=images_frame
        self.location_frame = location_frame
        self.annotation_frame = annotation_frame
        self.io_frame = io_frame

        self.initUI()

    def initUI(self):

        # Grid definition
        self.grid = QGridLayout()
        self.grid.addWidget(self.images_frame, 0, 1, 3, 1)
        self.grid.addWidget(self.annotation_frame, 1, 0, 1, 1)
        self.grid.addWidget(self.location_frame, 2, 0, 1, 1)
        self.grid.addWidget(self.io_frame, 0, 0, 1, 1)

        self.setLayout(self.grid)
        self.setGeometry(300, 300, 500, 500)
        self.setWindowTitle('Feature curation GUI')
